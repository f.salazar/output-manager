#include <Arduino.h>
#include <OutputManager.h>

OutputManager om;
unsigned long startMillis = 0;
bool writeAction = false;

void setup()
{
  Serial.begin(115200);
  // READ PLEASE!!
  // MODE: LOW_LEVEL (WRITE 0 TO TURN ON AND WRITE 1 TO TURN OFF)
  // OR HIGH_LEVEL (WRITE 1 TO TURN ON AND WRITE 0 TO TURN OFF)
  // YOU EVER NEED USE 1 TO TURN ON OR 0 FOR TURN OFF BECAUSE THE LIBRARY MAKE CONVERSION AUTOMATICALLY

  // SETUP OUTPUT: 1 PIN: 16 MODE: LOW_LEVEL
  om.setupOutput(1, 16, LOW_LEVEL);

  // SETUP OUTPUT: 2 PIN: 17 MODE: LOW_LEVEL
  om.setupOutput(2, 17, LOW_LEVEL);

  Serial.println("Boot done");
  startMillis = millis();

  // WRITE TO OUTPUT: 1 VALUE: 1 WITHOUT DELAY
  om.write(1, 1);

  // WRITE TO OUTPUT: 2 VALUE: 1 WITHOUT DELAY
  om.write(2, 1);
}

void loop()
{
  om.outputsLoop();
  if (millis() - startMillis >= 3000 && !writeAction)
  {
    Serial.println("WRITE TEST READY");

    // WRITE TO OUTPUT: 1 VALUE: 0 WITH DELAY 5 SECONDS
    om.write(1, 0, 5000);

    // WRITE TO OUTPUT: 2 VALUE: 0 WITH DELAY 10 SECONDS
    om.write(2, 0, 10000);

    writeAction = true;
  }
}