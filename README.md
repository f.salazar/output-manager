
# Output manager library for Arduino/ESP32

Output manager library for Arduino or ESP32 for control outputs using LOW LEVEL TRIGGER or HIGH LEVEL TRIGGER, with easy functions to write value with and without delay, write in force mode for overriding write normal order and writeRelaunch for write value and before time write again other value to same output 

## Requirements

The following are needed 

* [Arduino](http://www.arduino.cc) or [ESP32](https://www.espressif.com/en/products/socs/esp32)
* Connects cable between RELAY or WATEVER (compatible circuit) you want control using output of arduino or esp32

## Installation 

Create a folder named Wiegand in Arduino's libraries folder.  You will have the following folder structure:

	cd arduino/libraries
	git clone https://gitlab.com/f.salazar/output-manager.git OutputManager


*This library is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation; either version 2.1 of the License, or (at your option) any later version.*

*This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.*
