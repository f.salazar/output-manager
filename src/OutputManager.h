#include "Arduino.h"

#ifndef _OUTPUT_MANAGER_H
#define _OUTPUT_MANAGER_H

enum OutputMode
{
  HIGH_LEVEL,
  LOW_LEVEL
};

class OutputManager
{
  typedef struct OutputConfig
  {
    byte pin;
    byte output;
    unsigned long onTime = 0;
    byte value;
    byte relaunchValue;
    uint16_t relaunchBefore;
    uint16_t level;
    uint16_t before;
    bool relaunch = false;
    bool pending = false;
    bool available = true;
    void (*writeCallback)();
  } OutputConfig_t;

private:
  static int OUTPUTS_COUNT;
  static void _evaluatePendingWrite(byte output);
  static int _parseWriteValue(byte output, byte value);
  static bool _evaluateWriteValue(byte output, byte value);
  static void _setRelaunchConfig(byte output);
  static OutputConfig_t _outputConfig[10];

public:
  OutputManager(uint8_t outputs = 1);
  void setupOutput(byte output, byte pin, byte level = LOW_LEVEL);
  void write(byte output, byte value, uint16_t before = 0);
  void writeRelaunch(byte output, byte value, uint16_t before, byte relaunchValue, uint16_t relaunchBefore);
  void writeForce(byte output, byte value);
  unsigned long convertSecondsToMillis(uint16_t seconds);
  uint16_t convertMillisToSeconds(unsigned long millis);
  byte readOutputState(byte output);
  byte readLastWrite(byte output);
  void enableOutput(byte output);
  void disableOutput(byte output);
  void outputsLoop();
};

#endif