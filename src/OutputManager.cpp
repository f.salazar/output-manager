#include "OutputManager.h"

int OutputManager::OUTPUTS_COUNT;

OutputManager::OutputConfig_t OutputManager::_outputConfig[10];

OutputManager::OutputManager(uint8_t outputs)
{
  OUTPUTS_COUNT = outputs;
}

void OutputManager::_evaluatePendingWrite(byte output)
{
  if (_outputConfig[output].pending)
  {
    unsigned long onTime = _outputConfig[output].onTime;
    uint16_t targetTime = _outputConfig[output].before;
    unsigned long currentTime = millis();

    if (currentTime - onTime >= targetTime)
    {
      byte value = _parseWriteValue(output, _outputConfig[output].value);
      byte pin = _outputConfig[output].pin;
      digitalWrite(pin, value);

      if (_outputConfig[output].relaunch)
      {
        _setRelaunchConfig(output);
        return;
      }

      _outputConfig[output].pending = false;
    }
  }
}

bool OutputManager::_evaluateWriteValue(byte output, byte value)
{
  byte write = _parseWriteValue(output, value);
  byte current = _parseWriteValue(output, _outputConfig[output].value);

  if (write == current)
  {
    Serial.println("WRITE VALUE: " + String(value) + " IS SAME TO CURRENT VALUE: " + String(current));
    return false;
  }

  return true;
}

int OutputManager::_parseWriteValue(byte output, byte value)
{
  byte level = _outputConfig[output].level;

  if ((value == 1 || value == 0) && level == LOW_LEVEL)
  {
    return !value;
  }
  
  if (value > 1 || value < 0)
  {
    Serial.println("WARNING WRITE VALUE: " + String(value) + " IS INVALID PLEASE WRITE 1 OR 0");
    return 0;
  }

  return value;
}

void OutputManager::_setRelaunchConfig(byte output)
{
  if (!_outputConfig[output].relaunch)
    return;

  byte relaunchValue = _outputConfig[output].relaunchValue;
  uint16_t relaunchTime = _outputConfig[output].relaunchBefore;
  _outputConfig[output].onTime = millis();
  _outputConfig[output].value = relaunchValue;
  _outputConfig[output].before = relaunchTime;
  _outputConfig[output].relaunch = false;
  _outputConfig[output].pending = true;
}

void OutputManager::setupOutput(byte output, byte pin, byte level)
{
  _outputConfig[output].output = output;
  _outputConfig[output].pin = pin;
  _outputConfig[output].level = level;
  _outputConfig[output].value = level == LOW_LEVEL ? 0 : 1;
  pinMode(pin, OUTPUT);
  digitalWrite(pin, level);
}

void OutputManager::write(byte output, byte value, uint16_t before)
{
  bool writeAvailable = _evaluateWriteValue(output, value);
  if (!writeAvailable)
    return;

  byte pin = _outputConfig[output].pin;
  _outputConfig[output].value = value;

  if (before > 0)
  {
    _outputConfig[output].onTime = millis();
    _outputConfig[output].pending = true;
    _outputConfig[output].before = before;
    return;
  }

  byte parsedValue = _parseWriteValue(output, value);
  digitalWrite(pin, parsedValue);
  _setRelaunchConfig(output);
}

void OutputManager::writeRelaunch(byte output, byte value, uint16_t before, byte relaunchValue, uint16_t relaunchBefore)
{
  bool writeAvailable = _evaluateWriteValue(output, value);

  if (!writeAvailable)
    return;
    
  _outputConfig[output].relaunchValue = relaunchValue;
  _outputConfig[output].relaunchBefore = relaunchBefore;
  _outputConfig[output].relaunch = true;
  write(output, value, before);
}

void OutputManager::writeForce(byte output, byte value)
{
  byte pin = _outputConfig[output].pin;
  byte parsedValue = _parseWriteValue(output, value);
  _outputConfig[output].value = value;
  _outputConfig[output].pending = false;
  _outputConfig[output].relaunch = false;
  digitalWrite(pin, parsedValue);
  Serial.println("WRITE TO OUTPUT: " + String(output) + " FORCE VALUE: " + String(value) + " OVERRIDING PREVIOUS TASKS");
}

unsigned long OutputManager::convertSecondsToMillis(uint16_t seconds)
{
  return seconds * 1000;
}

uint16_t OutputManager::convertMillisToSeconds(unsigned long millis)
{
  return millis / 1000;
}

byte OutputManager::readOutputState(byte output)
{
  byte pin = _outputConfig[output].pin;
  byte readed = digitalRead(pin);
  byte value = _parseWriteValue(output, readed);
  return value;
}

byte OutputManager::readLastWrite(byte output)
{
  byte last = _outputConfig[output].value;
  byte value = _parseWriteValue(output, last);
  return value;
}

void OutputManager::outputsLoop()
{
  for (size_t i = 1; i <= OUTPUTS_COUNT; i++)
  {
    if (_outputConfig[i].available)
      _evaluatePendingWrite(i);
  }
}

void OutputManager::disableOutput(byte output)
{
  _outputConfig[output].available = false;
}

void OutputManager::enableOutput(byte output)
{
  _outputConfig[output].available = true;
}